const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Campground = require('./models/campground');
const Comments = require('./models/comment');
const seedDB = require('./seeds');

//app.use
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({
  extended: true
}));
//connect mongoose
mongoose.connect('mongodb://localhost:27017/yelpCamp', {
  useNewUrlParser: true
});
//setup view and ejs
app.set('view engine', 'ejs');
//seed the db
seedDB();

app.get('/', function(req, res) {
  res.render("landing");
});

app.get('/campgrounds', function(req, res) {
  //Get all campgrounds from db
  Campground.find({}, function(err, campgrounds) {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/index', {
        campgrounds: campgrounds
      });
    }
  });
});

app.post('/campgrounds', function(req, res) {
  //get data from form and add to campgrounds array
  //redirect back to campgrounds page
  var name = req.body.name;
  var image = req.body.image;
  var desc = req.body.description
  var newCampground = {
    name: name,
    image: image,
    description: desc
  };
  //create new campground and safe to database
  Campground.create(newCampground, function(err, newlyCreated) {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/campgrounds');
    }
  })
});

app.get('/campgrounds/new', function(req, res) {
  res.render("campgrounds/new");
});
//Show - shows more info about one campground
app.get('/campgrounds/:id', function(req, res) {
  //find the campground with provided id
  Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
    if (err) {
      console.log(err);
    } else {
      res.render("campgrounds/show", {
        campground: foundCampground
      });
    }
  });
});

// ==================
//Comments Routes
// ==================
app.get('/campgrounds/:id/comments/new', function(req, res) {
  Campground.findById(req.params.id, function(err, campground) {
    if (err) {
      console.log(err);
    } else {
      res.render("comments/new", { campground: campground});
    }
  });
});

app.post('/campgrounds/:id/comments', function(req,res){
  Campground.findById(req.params.id, function(err, campground) {
    if(err){
      console.log(err);
      res.redirect("/campgrounds");
    }else{
      Comments.create(req.body.comment, function(err, comment){
        if(err){
          console.log(err);
        }else{
          campground.comments.push(comment);
          campground.save();
          res.redirect("/campgrounds/" + campground._id);
        }
      });
    }
  });
});


app.listen(3000, function() {
  console.log("The YelpCamp Server Started");
});
